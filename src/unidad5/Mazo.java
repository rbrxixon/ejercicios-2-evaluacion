package unidad5;

import java.util.ArrayList;
import java.util.Random;

import unidad5.Naipe.CartaEnum;
import unidad5.Naipe.PaloEnum;

public class Mazo {
	ArrayList<Naipe> naipes;
	
	protected Mazo() {
		// Crear 52 cartas, 13 naipes de cada palo
		super();
		setNaipes(crearTodasCartas());
	}
	
	protected Mazo(int n) {
		// Crear n cartas al azar
		super();
		setNaipes(crearCartas(n));
	}
	
	protected Mazo(String juego) {
		super();
		if (juego.equals("Blackjack")) {
			setNaipes(crearTodasCartasBJ());
		}
	}
	
	private ArrayList<Naipe> crearCartas(int n) {
		ArrayList<Naipe> naipes = new ArrayList<Naipe>();
		for (int i = 0; i < n; i++) {
			naipes.add(new Naipe(PaloEnum.getRandom(), CartaEnum.getRandom()));
		}
		return naipes;
	}
	
	private ArrayList<Naipe> crearTodasCartas() {
		ArrayList<Naipe> naipes = new ArrayList<Naipe>();
		for (Naipe.PaloEnum palo : Naipe.PaloEnum.values() ) {
			for (Naipe.CartaEnum carta : Naipe.CartaEnum.values() ) {
			//System.out.println(carta + " " + palo);
			naipes.add(new Naipe(palo,carta));
			}
		}
		return naipes;
	}
	
	private ArrayList<Naipe> crearTodasCartasBJ() {
		ArrayList<Naipe> naipes = new ArrayList<Naipe>();
		System.out.println("Jugando a blackjack");
		Naipe n;
		for (Naipe.PaloEnum palo : Naipe.PaloEnum.values() ) {
			for (Naipe.CartaEnum carta : Naipe.CartaEnum.values() ) {
			//System.out.println(carta + " " + palo);
				if (carta == CartaEnum.CARTA_ACE) {
					n = new Naipe(palo,carta,1);
				} else if ((carta == CartaEnum.CARTA_JACK) || (carta == CartaEnum.CARTA_QUEEN) || (carta == CartaEnum.CARTA_KING)) {
					n = new Naipe(palo,carta,10);
				} else {
					n = new Naipe(palo,carta);
				}
			naipes.add(n);
			}
		}
		return naipes;
	}
	 
	private ArrayList<Naipe> getNaipes() {
		return naipes;
	}

	private void setNaipes(ArrayList<Naipe> naipes) {
		this.naipes = naipes;
	}

	@Override
	public String toString() {
		return "Mazo [naipes=" + naipes + "]";
	}
	
	public Naipe get() {
		System.out.println("Retornar un naipe elegido de forma aleatoria");
		return getNaipes().get(new Random().nextInt(getNaipes().size()));
	}
	
	public Naipe remove() {
		System.out.println("Retirar del mazo un naipe elegido de forma aleatoria");
		return getNaipes().remove(new Random().nextInt(getNaipes().size()));
	}
	
	public void add(Naipe naipe) {
		System.out.println("Añadir un naipe al mazo " + naipe.toString());
		getNaipes().add(naipe);
	}
	
	public void addAll(Mazo mazo) {
		System.out.println("Añadir al mazo los naipes almacenados en otro mazo");
		System.out.println("Mazo a insertar " + mazo.getNaipes().size());
		System.out.println("Mazo a recibir naipes " + getNaipes().size());
		getNaipes().addAll(mazo.getNaipes());
		System.out.println("Eliminar los naipes del mazo original");
		mazo.getNaipes().clear();
		System.out.println("Mazo a recibir naipes " + getNaipes().size());
		System.out.println("Mazo orignal del quie se ha insertado " + mazo.getNaipes().size());
	}
	
	private boolean comprobarCartaMasAlta(Naipe jugador1, Naipe jugador2) {
		System.out.println("Jugador1: " + jugador1.toString());
		System.out.println("Jugador2: " + jugador2.toString());
		if (jugador1.getValor() > jugador2.getValor()) {
			System.out.println("Ha ganado jugador 1");
			return false;
		} else if (jugador2.getValor() > jugador1.getValor()) {
			System.out.println("Ha ganado jugador 2");
			return false;
		} else if (jugador1.getValor() == jugador2.getValor()) {
			System.out.println("Han empatado el juego");
			System.out.println("Volver a repartir 2 cartas");
			return true;
		} else {
			System.out.println("Error");
			return true;
		}
	}
	
	// Generamos un mazo estandard, 
	// Damos 1 carta a cada jugador. 
	// La carta mas alta ganara
	// En caso de empate, se da otras 2 cartas a cada jugador. 

	public static void main(String[] args) {
	/*
		Mazo m = new Mazo();
		System.out.println(m.toString());
		
		Mazo m2 = new Mazo(33);
		System.out.println(m2.toString());
		System.out.println(m2.get());
		System.out.println(m2.getNaipes().size());
		System.out.println(m2.remove());
		System.out.println(m2.getNaipes().size());
		m2.add(new Naipe());
		System.out.println(m2.getNaipes().size());
		Mazo m3 = new Mazo(5);
		m2.addAll(m3);
 	*/
		Mazo mazo = new Mazo();
		do {
			System.out.println("Vamos a repartir cartas");
		} while (mazo.comprobarCartaMasAlta(mazo.get(),mazo.get()));
		
	}



}
