package unidad5;

import java.util.ArrayList;
import java.util.Collection;

public class Libro {
	String titulo;
	Collection<Autorr> autores;
	float precio;
	int stock;

	
	public Libro(String titulo, Collection<Autorr> autores, float precio) {
		super();
		this.titulo = titulo;
		this.autores = autores;
		this.precio = precio;
		this.stock = 1;
	}
	
	public Libro(String titulo, Collection<Autorr> autores, float precio, int stock) {
		super();
		this.titulo = titulo;
		this.autores = autores;
		this.precio = precio;
		this.stock = stock;
	}

	public String getTitulo() {
		return titulo;
	}

	public Collection<Autorr> getAutores() {
		return autores;
	}

	public float getPrecio() {
		return precio;
	}

	public int getStock() {
		return stock;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
	
	@Override
	public String toString() {
		return titulo + "(" + autores + ") " + precio + " € - " + stock + " unidades en stock";
	}

	public static void main(String[] args) {
		Collection<Autorr> autores = new ArrayList<Autorr>();
		autores.add(new Autorr("Perez Reverte"));
		Libro libro = new Libro("Linea de fuego", autores, 500);
		System.out.println(libro.toString());
		
		
		Collection<Autorr> autores2 = new ArrayList<Autorr>();
		autores2.add(new Autorr("Anna Frank"));
		autores2.add(new Autorr("Otto Heinrich Frank"));
		Libro libro2 = new Libro("Diario de Anna Frank", autores2, 200, 20);
		System.out.println(libro2.toString());
	}

}

class Autorr {
	String nombre;
	
	public Autorr(String nombre) {
		///super();
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return nombre + " ";
	}
	
}