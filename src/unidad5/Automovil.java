package unidad5;

import java.util.ArrayList;

public class Automovil {
	String modelo;
	double depositoCapacidad;
	double depositoCantidadActual;
	double consumoPorKilometro;
	double totalKilometrosRecorridos;
	double totalCombustibleConsumido;
	
	public Automovil(String modelo, double depositoCapacidad, double depositoCantidadActual, double consumoPorKilometro) {
		super();
		this.modelo = modelo;
		this.depositoCapacidad = depositoCapacidad;
		this.depositoCantidadActual = depositoCantidadActual;
		this.consumoPorKilometro = consumoPorKilometro;
		this.totalKilometrosRecorridos = 0;
		this.totalCombustibleConsumido = 0;
	}
	
	public Automovil(String modelo, double depositoCapacidad, double consumoPorKilometro) {
		super();
		this.modelo = modelo;
		this.depositoCapacidad = depositoCapacidad;
		this.consumoPorKilometro = consumoPorKilometro;
		this.depositoCantidadActual = depositoCapacidad;
		this.totalKilometrosRecorridos = 0;
		this.totalCombustibleConsumido = 0;
	}
	
	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public double getDepositoCapacidad() {
		return depositoCapacidad;
	}

	public void setDepositoCapacidad(double depositoCapacidad) {
		this.depositoCapacidad = depositoCapacidad;
	}

	public double getDepositoCantidadActual() {
		return depositoCantidadActual;
	}

	public void setDepositoCantidadActual(double depositoCantidadActual) {
		this.depositoCantidadActual = depositoCantidadActual;
	}

	public double getConsumoPorKilometro() {
		return consumoPorKilometro;
	}

	public void setConsumoPorKilometro(double consumoPorKilometro) {
		this.consumoPorKilometro = consumoPorKilometro;
	}

	public double getTotalKilometrosRecorridos() {
		return totalKilometrosRecorridos;
	}

	public void setTotalKilometrosRecorridos(double totalKilometrosRecorridos) {
		this.totalKilometrosRecorridos = totalKilometrosRecorridos;
	}

	public double getTotalCombustibleConsumido() {
		return totalCombustibleConsumido;
	}

	public void setTotalCombustibleConsumido(double totalCombustibleConsumido) {
		this.totalCombustibleConsumido = totalCombustibleConsumido;
	}
	
	@Override
	public String toString() {
		return "Automovil [modelo=" + modelo + ", depositoCapacidad=" + depositoCapacidad + ", depositoCantidadActual="
				+ depositoCantidadActual + ", consumoPorKilometro=" + consumoPorKilometro
				+ ", totalKilometrosRecorridos=" + totalKilometrosRecorridos + ", totalCombustibleConsumido="
				+ totalCombustibleConsumido + "]";
	}

	private void llenarDeposito() {
		System.out.println("Se llena el deposito hasta el limite");
		setDepositoCantidadActual(getDepositoCapacidad());
	}
	
	private void llenarDeposito(double cantidad) {
		System.out.println("Se llena el deposito con una cantidad de litros especifica");
		if (getDepositoCantidadActual() + cantidad > getDepositoCapacidad()) {
			System.out.println("Excede la capacidad del deposito, se llenara el deposito retornando la cantidade de combustible sobrante");
			setDepositoCantidadActual(getDepositoCapacidad());
		} else {
			System.out.println("Se llenaran " + cantidad + " litros");
			setDepositoCantidadActual(getDepositoCantidadActual() + cantidad);
		}
	}
	
	private void desplazar(double kilometrosRecorrer) {
		double litrosAGastar = kilometrosRecorrer * getConsumoPorKilometro();
		System.out.println("Desplazar " + getModelo() + " " + kilometrosRecorrer);
		if (getDepositoCantidadActual() >= litrosAGastar) {
			System.out.println("Es posible moverse con los litros actuales");
			System.out.println("Deposito Actual: " + getDepositoCantidadActual() + " KilometrosARecorrer " + kilometrosRecorrer + " LitrosAGastar " + litrosAGastar);
			setDepositoCantidadActual(getDepositoCantidadActual() - litrosAGastar);
			System.out.println("Deposito Actual: " + getDepositoCantidadActual() + " LitrosAGastar " + litrosAGastar);
			setTotalCombustibleConsumido(litrosAGastar);
			setTotalKilometrosRecorridos(kilometrosRecorrer);
		} else {
			System.out.println("Combustible insuficiente para este desplazamiento");
			System.out.println("Deposito Actual: " + getDepositoCantidadActual() + " KilometrosARecorrer " + kilometrosRecorrer + " LitrosAGastar " + litrosAGastar); 
		}
	}

	public static void main(String[] args) {
		ArrayList<Automovil> vehiculos = new ArrayList<Automovil>();
		Automovil a1 = new Automovil("AudiA4", 80, 23.0, 0.3);
		Automovil a2 = new Automovil("BMW-M2", 75, 45, 0.42);
		vehiculos.add(a1);
		vehiculos.add(a2);
		
		System.out.println(vehiculos.size());
		a1.desplazar(40);

	}

}
