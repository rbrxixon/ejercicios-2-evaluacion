package unidad5;

public class Autor {
	String nombre;
	String email;
	String genero;

	public Autor(String nombre, String email, String genero) {
		super();
		this.nombre = nombre;
		this.email = email;
		this.genero = genero;
	}
	

	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getGenero() {
		return genero;
	}


	public void setGenero(String genero) {
		this.genero = genero;
	}
	

	@Override
	public String toString() {
		return getNombre() + " (" + getGenero() + ") " + getEmail() ;
	}


	public static void main(String[] args) {
		Autor autor = new Autor("Paco", "paco@paqkito.es", "masculino");
		System.out.println(autor.toString());
		
		autor.setEmail("test@test.es");
		autor.setGenero("femenino");
		autor.setNombre("Pepita");
		System.out.println(autor.toString());

	}

}
