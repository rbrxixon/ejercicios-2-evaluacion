package unidad5;

import java.time.LocalDate;

public class Animal {

	String nombre;
	LocalDate fecha;
	
	public Animal(String nombre, LocalDate fecha) {
		super();
		this.nombre = nombre;
		this.fecha = fecha;
	}
	
	

	public Animal(String nombre) {
		super();
		this.nombre = nombre;
		this.fecha = LocalDate.now();
	}

	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public LocalDate getFecha() {
		return fecha;
	}



	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}



	@Override
	public String toString() {
		return "Nombre: " + nombre + " - Fecha: " + fecha ;
	}



	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Animal animal1 = new Animal("Gato");
		System.out.println("Nombre: " + animal1.getNombre() + " - Fecha: " + animal1.getFecha());
		
		Animal animal2 = new Animal("Perro",LocalDate.of(2021,02,11));
		System.out.println(animal2.toString());
		
		Animal animal3 = new Animal("Tortuga");
		animal3.setFecha(LocalDate.of(1999, 02, 02));
		animal3.setNombre("Pez");
		System.out.println(animal3.toString());
	}

}
