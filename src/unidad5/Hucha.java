package unidad5;

import java.util.ArrayList;
import java.util.Iterator;


public class Hucha {

	boolean huchaAbierta; // Abierta - Cerrada
	String password;
	ArrayList<Moneda> monedas;
	ArrayList<Billete> billetes;
	int totalCantidades;
	
	public Hucha() {
		super();
		this.password = "hola";
		this.monedas = new ArrayList<Moneda>();
		this.billetes = new ArrayList<Billete>();
		this.totalCantidades = 0;
		this.huchaAbierta = true;
	}
	
	public Hucha(ArrayList<Moneda> _monedas, ArrayList<Billete> _billetes) {
		super();
		this.password = "hola";
		this.monedas = _monedas;
		this.billetes = _billetes;
		this.totalCantidades = 0;
		this.huchaAbierta = true;
		
		for (Billete billete : getBilletes()) {
			setTotalCantidades(getTotalCantidades() + billete.getCantidad());
		}
		
		for (Moneda moneda : getMonedas()) {
			setTotalCantidades(getTotalCantidades() + moneda.getCantidad());
		}
	}
	
	public Hucha(int cantidad) {
		super();
		this.password = "hola";
		this.monedas = new ArrayList<Moneda>();
		this.billetes = new ArrayList<Billete>();
		this.totalCantidades = 0;
		this.huchaAbierta = true;
		int cantidadRestante = cantidad;
		
		while (cantidadRestante > 0) {
			if (cantidadRestante >= 50) {
				billetes.add(new Billete(50));
				cantidadRestante-=50;
				setTotalCantidades(getTotalCantidades() + 50);
			} else if (cantidadRestante >= 20) {
				billetes.add(new Billete(20));
				cantidadRestante-=20;
				setTotalCantidades(getTotalCantidades() + 20);
			} else if (cantidadRestante >= 10) {
				billetes.add(new Billete(10));
				cantidadRestante-=10;
				setTotalCantidades(getTotalCantidades() + 10);
			} else if (cantidadRestante >= 5) {
				billetes.add(new Billete(5));
				cantidadRestante-=5;
				setTotalCantidades(getTotalCantidades() + 5);
			} else if (cantidadRestante >= 2) {
				monedas.add(new Moneda(2));
				cantidadRestante-=2;
				setTotalCantidades(getTotalCantidades() + 2);
			} else if (cantidadRestante >= 1) {
				monedas.add(new Moneda(1));
				cantidadRestante-=1;
				setTotalCantidades(getTotalCantidades() + 1);
			}
		}

	}
	
	public boolean isHuchaAbierta() {
		return huchaAbierta;
	}

	public void setHuchaAbierta(boolean estado) {
		this.huchaAbierta = estado;
	}
	
	public void abrirHucha(String password) {
		if (password == getPassword()) {
		System.out.println("Password correcta");
			if (this.huchaAbierta) {
				System.out.println("Hucha abierta");
				setHuchaAbierta(true);
		} else {
				System.out.println("Hucha cerrada");
				setHuchaAbierta(false);
				}
		} else {
			System.out.println("Password incorrecta");
		}
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getTotalCantidades() {
		return totalCantidades;
	}

	public void setTotalCantidades(int totalCantidades) {
		this.totalCantidades = totalCantidades;
	}
	
	public ArrayList<Moneda> getMonedas() {
		return monedas;
	}

	public void setMonedas(ArrayList<Moneda> monedas) {
		this.monedas = monedas;
	}

	public ArrayList<Billete> getBilletes() {
		return billetes;
	}

	public void setBilletes(ArrayList<Billete> billetes) {
		this.billetes = billetes;
	}

	public void meterDinero (Moneda dinero) {
		System.out.println("Introducida moneda " + dinero.getCantidad());
		monedas.add(dinero);
		totalCantidades+=dinero.cantidad;
	}
	
	public void meterDinero (Billete dinero) {
		System.out.println("Introducido Billete " + dinero.getCantidad());
		billetes.add(dinero);
		totalCantidades+=dinero.cantidad;
	}
	
	public void retirarDinero (String tipo, int cantidad, int valor) {
		//System.out.println("Retirada tipo: " + tipo + " Cantidad: " + cantidad + " Valor: " + valor);
		int cantidadRetirados = 0;
		
		if (tipo == "Billete") {
			Iterator<Billete> i = getBilletes().iterator();
			while (i.hasNext()) {
				Billete billete = i.next();
				if (cantidadRetirados >= cantidad)
					break;
			
				if (valor == billete.getCantidad()) {
					System.out.println("Retirada efectuada tipo: " + tipo + " cantidad: " + cantidad + " valor: " + valor);
					i.remove();
					setTotalCantidades((getTotalCantidades()-valor));
					cantidadRetirados++;
					System.out.println(toString());
				}	
			}
			
		} else if (tipo == "Moneda") {
			Iterator<Moneda> i = getMonedas().iterator();
			while (i.hasNext()) {
				Moneda moneda = i.next();
				if (cantidadRetirados >= cantidad)
					break;
				
				if (valor == moneda.getCantidad()) {
					System.out.println("Retirada efectuada tipo: " + tipo + " cantidad: " + cantidad + " valor: " + valor);
					i.remove();
					setTotalCantidades((getTotalCantidades()-valor));
					cantidadRetirados++;
					System.out.println(toString());
				}	
			}
		} else {
			System.out.println("Tipo desconocido");
			}	
	}
		
	public boolean comprobarSiHayDinero(int valor) {
		boolean hayDinero = false;
		
		if (valor == 5 || valor == 10 || valor == 20 || valor == 50) {
			
			for (Billete billete : getBilletes()) {
				if (valor == billete.getCantidad()) {
					System.out.println("Hay Billete de " + billete.getCantidad() );
					hayDinero = true;
					break;
				} else {
					hayDinero = false;
				}
			}
		} else if (valor == 1 || valor == 2 ){
			for (Moneda moneda : getMonedas()) {
				if (valor == moneda.getCantidad()) {
					System.out.println("Hay Moneda de " + moneda.getCantidad() );
					hayDinero = true;
					break;
				} else {
					hayDinero = false;
				}
			}
		} else {
			System.out.println("Cantidad introducida incorrecta");
		}
			return hayDinero;
	}
	
	public void retirarDinero (int cantidad) {
		System.out.println("Retirar dinero " + cantidad);
		int cantidadRestante = cantidad;
		boolean b50,b20,b10,b5,m2,m1;
		b50 = b20 = b10 = b5 = m2 = m1 = true;
		
		while (cantidadRestante > 0) {
			if (b50 && cantidadRestante >= 50) {
				if (comprobarSiHayDinero(50)) {
					System.out.println("Retirar billete de 50");	
					cantidadRestante-=50;
					retirarDinero("Billete", 1, 50);
				} else {
					System.out.println("No hay billete de 50");
					b50 = false;
				}
			} else if ((b20 && cantidadRestante >= 20) || b50 == false && cantidadRestante >= 50 && b20) {
				if (comprobarSiHayDinero(20)) {
					System.out.println("Retirar billete de 20");	
					cantidadRestante-=20;
					retirarDinero("Billete", 1, 20);
				} else {
					System.out.println("No hay billete de 20");
					b20 = false;
				}
			} else if ((b10 && cantidadRestante >= 10) || b20 == false && cantidadRestante >= 20 && b10) {
				if (comprobarSiHayDinero(10)) {
					System.out.println("Retirar billete de 10");	
					cantidadRestante-=10;
					retirarDinero("Billete", 1, 10);
				} else {
					System.out.println("No hay billete de 10");
					b10 = false;
				}
			} else if ((b5 && cantidadRestante >= 5) || b10 == false && cantidadRestante >= 10 && b5) {
				if (comprobarSiHayDinero(5)) {
					System.out.println("Retirar billete de 5");	
					cantidadRestante-=5;
					retirarDinero("Billete", 1, 5);
				} else {
					System.out.println("No hay billete de 5");
					b5 = false;
				}
			} else if ((m2 && cantidadRestante >= 2) || b5 == false && cantidadRestante >= 5 && m2) {
				if (comprobarSiHayDinero(2)) {
					System.out.println("Retirar de 2");	
					cantidadRestante-=2;
					retirarDinero("Moneda", 1, 2);
				} else {
					System.out.println("No hay moneda de 2");
					m2 = false;
				}
			} else if ((m1 && cantidadRestante >= 1) || m2 == false && cantidadRestante >= 2 && m1) {
				if (comprobarSiHayDinero(1)) {
					System.out.println("Retirar de 1");	
					cantidadRestante-=1;
					retirarDinero("Moneda", 1, 1);
				} else {
					System.out.println("No hay moneda de 1");
					m1 = false;
				}
			} else if (cantidadRestante == 0) {
				System.out.println("Todo retirado");
			} else {
				System.out.println("No queda dinero en la hucha");
				break;
			}
			//System.out.println("Cantidad Restante " + cantidadRestante);
		}
	}
	
	@Override
	public String toString() {
		return "Hucha [huchaAbierta=" + huchaAbierta + ", password=" + password + ", Monedas=" + monedas + ", Billetes="
				+ billetes + ", totalCantidades=" + totalCantidades + "]";
	}

	public static void main(String[] args) {
		Hucha hucha = new Hucha(255);
		hucha.abrirHucha("hola");
		System.out.println(hucha.toString());
		hucha.retirarDinero(55);
		
		/*
		ArrayList<Moneda> monedas = new ArrayList<Moneda>();
		ArrayList<Billete> billetes = new ArrayList<Billete>();
		monedas.add(new Moneda(1));
		monedas.add(new Moneda(2));
		monedas.add(new Moneda(2));
		billetes.add(new Billete(10));
		billetes.add(new Billete(50));
		billetes.add(new Billete(20));
		billetes.add(new Billete(5));
		Hucha hucha = new Hucha(monedas, billetes);
		hucha.abrirHucha("hola");
		System.out.println(hucha.toString());
		hucha.retirarDinero(55);
		*/
	
		/*
		Hucha hucha = new Hucha();
		hucha.abrirHucha("hola");
		hucha.meterDinero(new Moneda(2));
		hucha.meterDinero(new Billete(10));
		hucha.meterDinero(new Moneda(1));
		hucha.meterDinero(new Moneda(1));
		hucha.meterDinero(new Moneda(1));
		hucha.meterDinero(new Billete(10));
		hucha.meterDinero(new Billete(20));
		hucha.meterDinero(new Billete(20));
		//hucha.meterDinero(new Billete(50));
		//hucha.retirarDinero("Billete",2, 20);
		//hucha.retirarDinero("Moneda",1, 1);
		hucha.retirarDinero(55);
		*/
	}
}

class Moneda {
	int cantidad;

	public Moneda(int cantidad) {
		super();
		if (cantidad == 1 || cantidad == 2) {
			this.cantidad = cantidad;
			//System.out.println("Moneda introducida de: " + cantidad);
		} else {
			System.out.println("No se ha podido crear moneda");
		}
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public String toString() {
		return "Moneda " + cantidad;
	}
}

class Billete {
	int cantidad;

	public Billete(int cantidad) {
		super();
		if (cantidad == 5 || cantidad == 10 || cantidad == 20 || cantidad == 50) {
			this.cantidad = cantidad;
			//System.out.println("Billete introducida de: " + cantidad);
		} else {
			System.out.println("No se ha podido crear billete");
		}
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public String toString() {
		return "Billete " + cantidad;
	}
	
}
