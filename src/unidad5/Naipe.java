package unidad5;

public class Naipe {
	
	enum PaloEnum {
		TREBOLES (1),
		DIAMANTES (2),
		CORAZONES (3),
		PICAS (4);

		private final int paloCodigo;
		
		PaloEnum(int paloCodigo) {
			this.paloCodigo = paloCodigo;
		}

		public int getPaloCodigo() {
			return paloCodigo;
		}
		
		public static PaloEnum getRandom() {
			return values()[(int) (Math.random() * values().length)];
		}
	}
	
	enum CartaEnum {
		CARTA2 (2),
		CARTA3 (3),
		CARTA4 (4),
		CARTA5 (5),
		CARTA6 (6),
		CARTA7 (7),
		CARTA8 (8),
		CARTA9 (9),
		CARTA10 (10),
		CARTA_JACK (11),
		CARTA_QUEEN (12),
		CARTA_KING (13),
		CARTA_ACE (14);
			
		private final int cartaCodigo;
		
		CartaEnum(int cartaCodigo) {
			this.cartaCodigo = cartaCodigo;
		}
		
		public int getCartaCodigo() {
			return cartaCodigo;
		}
		
		public static CartaEnum getRandom() {
			return values()[(int) (Math.random() * values().length)];
		}			
	}
	
	PaloEnum paloEnum;
	CartaEnum cartaEnum;
	int valor;
	boolean cartaVista = false;
	
	public Naipe(PaloEnum paloEnum, CartaEnum cartaEnum, int valor) {
		super();
		this.paloEnum = paloEnum;
		this.cartaEnum = cartaEnum;
		this.valor = valor;
		this.cartaVista = false;
	}
	
	public Naipe(PaloEnum paloEnum, CartaEnum cartaEnum) {
		super();
		this.paloEnum = paloEnum;
		this.cartaEnum = cartaEnum;
		this.valor = cartaEnum.cartaCodigo;
	}
	
	public Naipe() {
		super();
		this.paloEnum = PaloEnum.getRandom();
		this.cartaEnum = CartaEnum.getRandom();
		this.valor = cartaEnum.cartaCodigo;
	}

	public PaloEnum getPaloEnum() {
		return paloEnum;
	}

	public void setPaloEnum(PaloEnum paloEnum) {
		this.paloEnum = paloEnum;
	}

	public CartaEnum getCartaEnum() {
		return cartaEnum;
	}

	public void setCartaEnum(CartaEnum cartaEnum) {
		this.cartaEnum = cartaEnum;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
		
	public boolean isCartaVista() {
		return cartaVista;
	}

	public void setCartaVista(boolean cartaVista) {
		this.cartaVista = cartaVista;
	}

	@Override
	public String toString() {
		return "Naipe [paloEnum=" + paloEnum + ", cartaEnum=" + cartaEnum + ", valor=" + valor + ", cartaVista="
				+ cartaVista + "]";
	}

	/*
	public static void main(String[] args) {
		Naipe n = new Naipe(PaloEnum.CORAZONES, CartaEnum.CARTA4);
		System.out.println(n.toString());
		
		Naipe n2 = new Naipe(PaloEnum.CORAZONES, CartaEnum.CARTA_KING, 233);
		System.out.println(n2.toString());

	}
	*/

}
