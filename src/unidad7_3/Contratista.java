package unidad7_3;

import java.util.ArrayList;

public class Contratista extends Empleado {
	
	private ArrayList<SociedadAnonima> sociedadesAnonimas;
		
	public Contratista(String nombre, String apellidos, int numeroCuentaBancaria,
			ArrayList<SociedadAnonima> sociedadesAnonimas) {
		super(nombre, apellidos, numeroCuentaBancaria);
		this.sociedadesAnonimas = sociedadesAnonimas;
	}

	public ArrayList<SociedadAnonima> getSociedadesAnonimas() {
		return sociedadesAnonimas;
	}

	public void setSociedadesAnonimas(ArrayList<SociedadAnonima> sociedadesAnonimas) {
		this.sociedadesAnonimas = sociedadesAnonimas;
	}

	@Override
	public String toString() {
		return "Contratista [nombre=" + nombre + ", apellidos=" + apellidos + ", fechaContratacion=" + fechaContratacion
				+ ", numeroCuentaBancaria=" + numeroCuentaBancaria + ", getSociedadesAnonimas()="
				+ getSociedadesAnonimas() + "]";
	}

	@Override
	public void realizarIngreso(int cantidad) {
		// TODO Auto-generated method stub
		System.out.println("Contratista, ingreso de " + cantidad + " realizado a cuenta bancaria");
	}
	
	
	
}
