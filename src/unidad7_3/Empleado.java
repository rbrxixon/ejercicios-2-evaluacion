package unidad7_3;

import java.time.LocalDate;

public abstract class Empleado implements Pagable {
	protected String nombre;
	protected String apellidos;
	protected LocalDate fechaContratacion;
	protected int numeroCuentaBancaria;
		
	public Empleado(String nombre, String apellidos, LocalDate fechaContratacion, int numeroCuentaBancaria) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.fechaContratacion = fechaContratacion;
		this.numeroCuentaBancaria = numeroCuentaBancaria;
	}
	
	public Empleado(String nombre, String apellidos, int numeroCuentaBancaria) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.fechaContratacion = LocalDate.now();
		this.numeroCuentaBancaria = numeroCuentaBancaria;
	}

	public String getNombre() {
		return nombre;
	}
	
	public String getApellidos() {
		return apellidos;
	}
	
	public LocalDate getFechaContratacion() {
		return fechaContratacion;
	}
	
	public int getNumeroCuentaBancaria() {
		return numeroCuentaBancaria;
	}

	public void setNumeroCuentaBancaria(int numeroCuentaBancaria) {
		this.numeroCuentaBancaria = numeroCuentaBancaria;
	}

}
