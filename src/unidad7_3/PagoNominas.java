package unidad7_3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class PagoNominas {
	private ArrayList<Empleado> empleados;
	ArrayList<EmpresaServicio> empresaServicio;
	
	public PagoNominas() {
		this.empleados = new ArrayList<>();
		this.empresaServicio = new ArrayList<EmpresaServicio>();
	}
	
	public ArrayList<Empleado> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(ArrayList<Empleado> empleados) {
		this.empleados = empleados;
	}

	public ArrayList<EmpresaServicio> getEmpresaServicio() {
		return empresaServicio;
	}

	public void setEmpresas(ArrayList<EmpresaServicio> empresaServicio) {
		this.empresaServicio = empresaServicio;
	}

	public static void main(String[] args) {
		PagoNominas pn = new PagoNominas();
		pn.getEmpleados().add(new Asalariado("Paco", "Revilla", 1234));
		
		ArrayList<String> trabajos = new ArrayList<String>();
		
		trabajos.add("Encofradores");
		SociedadAnonima sa = new SociedadAnonima("Sociedad de no cualificados", trabajos);
		ArrayList<SociedadAnonima> sociedades = new ArrayList<SociedadAnonima>();
		sociedades.add(sa);	
		//Se Añade un contratista cuando el array lo que acepta es objetos tipo Empleado
		pn.getEmpleados().add(new Contratista("Luis", "Menendez", 4567, sociedades));
		trabajos.clear();
		
		trabajos.add("Catering");
		pn.getEmpresaServicio().add(new EmpresaServicio("Servicios Paquita", trabajos, 1357));
		
		System.out.println("--------- METODO 1 ---------");
		
		//Se recorren los empleados y aparecen todos los tipos
		for (Empleado empleado : pn.getEmpleados()) {
			System.out.println(empleado.toString());
			empleado.realizarIngreso(100);
		}
		
		for (EmpresaServicio empresaServicio : pn.getEmpresaServicio()) {
			System.out.println(empresaServicio.toString());
			empresaServicio.realizarIngreso(1000);	
		}
		
		
		System.out.println("--------- METODO 2 ---------");
		ArrayList<Pagable> pagables = new ArrayList<Pagable>();
		pagables.addAll(pn.getEmpleados());
		pagables.addAll(pn.getEmpresaServicio());
		
		for (Pagable pagable : pagables) {
			System.out.println(pagable.toString());
			if (pagable instanceof Asalariado)
				pagable.realizarIngreso(500);
			else if (pagable instanceof Contratista)
				pagable.realizarIngreso(800);
			else if (pagable instanceof EmpresaServicio)
				pagable.realizarIngreso(2000);
		}
			
		System.out.println("--------- OPCIONAL ---------");
		ArrayList<Empleado> empleados2 = new ArrayList<>();
		
		Empleado e1 = new Asalariado("Luis", "Rodriguez", 12345);
		Empleado e2 = new Asalariado("Alfonso", "Perez", 425641);
		
		trabajos.add("Constructurores");
		trabajos.add("Basureros");
		SociedadAnonima sa2 = new SociedadAnonima("Sociedad de no cualificados", trabajos);
		ArrayList<SociedadAnonima> sociedades2 = new ArrayList<SociedadAnonima>();
		sociedades2.add(sa2);	
		Empleado e3 = new Contratista("Toni", "Soprano", 957843, sociedades2);
		pn.getEmpleados().add(e3);
		trabajos.clear();
		
		trabajos.add("Fontaneros");
		SociedadAnonima sa3 = new SociedadAnonima("Sociedad de fontaneros", trabajos);
		ArrayList<SociedadAnonima> sociedades3 = new ArrayList<SociedadAnonima>();
		sociedades3.add(sa3);	
		Empleado e4 = new Contratista("Junior", "Soprano", 957843, sociedades3);
		pn.getEmpleados().add(e4);
		trabajos.clear();
		
		empleados2.add(e1);
		empleados2.add(e2);
		empleados2.add(e3);
		empleados2.add(e4);
			
		for (Empleado empleado2 : empleados2) {
			System.out.println(empleado2.toString());
		}
		
		// No funciona
		//Collections.sort(empleados2);
		

	}

}
