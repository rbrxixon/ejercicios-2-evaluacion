package unidad7_3;

import java.time.LocalDate;

public class Asalariado extends Empleado {

	public Asalariado(String nombre, String apellidos, int numeroCuentaBancaria) {
		super(nombre, apellidos, numeroCuentaBancaria);
		// TODO Auto-generated constructor stub
	}
	
	public Asalariado(String nombre, String apellidos, LocalDate fechaContratacion, int numeroCuentaBancaria) {
		super(nombre, apellidos, fechaContratacion, numeroCuentaBancaria);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		return "Asalariado [getNombre()=" + getNombre() + ", getApellidos()=" + getApellidos()
				+ ", getFechaContratacion()=" + getFechaContratacion() + ", getNumeroCuentaBancaria()="
				+ getNumeroCuentaBancaria() + "]";
	}

	@Override
	public void realizarIngreso(int cantidad) {
		// TODO Auto-generated method stub
		System.out.println("Asalariado, ingreso de " + cantidad + " realizado a cuenta bancaria");
	}
	
	

}
