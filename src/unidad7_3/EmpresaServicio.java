package unidad7_3;

import java.util.ArrayList;

public class EmpresaServicio extends Empresa implements Pagable {
	private int numeroCuentaBancaria;
	

	public EmpresaServicio(String nombre, ArrayList<String> listaTrabajos, int numeroCuentaBancaria) {
		super(nombre, listaTrabajos);
		this.numeroCuentaBancaria = numeroCuentaBancaria;
	}

	public int getNumeroCuentaBarncaria() {
		return numeroCuentaBancaria;
	}

	public void setNumeroCuentaBarncaria(int numeroCuentaBarncaria) {
		this.numeroCuentaBancaria = numeroCuentaBarncaria;
	}

	@Override
	public String toString() {
		return "EmpresaServicio [numeroCuentaBarncaria=" + numeroCuentaBancaria + ", getNumeroCuentaBarncaria()="
				+ getNumeroCuentaBarncaria() + ", getNombre()=" + getNombre() + ", getListaTrabajos()="
				+ getListaTrabajos() + "]";
	}
	
	public void realizarIngreso(int cantidad) {
		System.out.println("EmpresaServicio, ingreso de " + cantidad + " realizado a cuenta bancaria");
	}
	
	
}
