package unidad7_3;

import java.util.ArrayList;

public abstract class Empresa {
	protected String nombre;
	private ArrayList<String> listaTrabajos;
	
	public Empresa(String nombre, ArrayList<String> listaTrabajos) {
		this.nombre = nombre;
		this.listaTrabajos = listaTrabajos;
	}

	public Empresa(String nombre) {
		super();
		this.nombre = nombre;
		this.listaTrabajos = new ArrayList<String>();
	}

	public String getNombre() {
		return nombre;
	}

	public ArrayList<String> getListaTrabajos() {
		return listaTrabajos;
	}
}
