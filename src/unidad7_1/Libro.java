package unidad7_1;

import java.util.ArrayList;

public class Libro extends Publicacion {
	private ArrayList<String> autores;
	boolean prestado;

	public Libro(int codigo, String titulo, int publicacion) {
		super(codigo, titulo, publicacion);
		autores = new ArrayList<String>();
		prestado = false;
		// TODO Auto-generated constructor stub
	}

	public ArrayList<String> getAutores() {
		return autores;
	}

	public void setAutores(ArrayList<String> autores) {
		this.autores = autores;
	}

	public boolean isPrestado() {
		return prestado;
	}

	public void setPrestado(boolean prestado) {
		this.prestado = prestado;
	}

	@Override
	public String toString() {
		return "Libro [" + codigo + ", " + titulo + ", " + publicacion + "]";
	}
	
}
