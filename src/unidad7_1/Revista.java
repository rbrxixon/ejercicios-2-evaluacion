package unidad7_1;

public class Revista extends Publicacion {
	private int numero;
	private int dia;
	private int mes;
	
	public Revista(int codigo, String titulo, int publicacion, int numero, int dia, int mes) {
		super(codigo, titulo, publicacion);
		this.numero = numero;
		this.dia = dia;
		this.mes = mes;
	}

	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public int getDia() {
		return dia;
	}
	
	public void setDia(int dia) {
		this.dia = dia;
	}
	
	public int getMes() {
		return mes;
	}
	
	public void setMes(int mes) {
		this.mes = mes;
	}

	@Override
	public String toString() {
		return "Revista [" + numero + ", " + dia + "/" + mes + ", " + codigo + ", " + titulo + ", " + publicacion + "]";
	}
	
}
