package unidad7_1;

public abstract class Publicacion {
	protected int codigo;
	protected String titulo;
	protected int publicacion;
	
	public Publicacion(int codigo, String titulo, int publicacion) {
		this.codigo = codigo;
		this.titulo = titulo;
		this.publicacion = publicacion;
	}

	public int getCodigo() {
		return codigo;
	}
	
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	public String getTitulo() {
		return titulo;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getPublicacion() {
		return publicacion;
	}
	
	public void setPublicacion(int publicacion) {
		this.publicacion = publicacion;
	}
}
