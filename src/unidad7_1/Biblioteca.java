package unidad7_1;

import java.util.ArrayList;

public class Biblioteca {
	private ArrayList<Publicacion> publicaciones;
	
	public Biblioteca() {
		publicaciones = new ArrayList<>();
	}
	
	public ArrayList<Publicacion> getPublicaciones() {
		return publicaciones;
	}

	public void setPublicaciones(ArrayList<Publicacion> publicaciones) {
		this.publicaciones = publicaciones;
	}

	public static void main(String[] args) {
		Biblioteca b = new Biblioteca();
		//Se añada al Array de publicacion los libros y revista, por lo que se esta usando el poliformismo de clases
		b.getPublicaciones().add(new Libro(1, "Hijos de la noche", 1984));
		b.getPublicaciones().add(new Revista(1, "Ola", 2020, 12, 12, 4));
			
		for (Publicacion publicacion : b.getPublicaciones()) {
			System.out.println(publicacion.toString());
		}
	}
}
