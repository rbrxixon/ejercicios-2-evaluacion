package unidad6_6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import unidad5.Naipe;

public class Blackjack {
	private ArrayList<Naipe> crupier;
	private ArrayList<ArrayList<Naipe>> jugadores;
	private boolean seguir = true;
	Mano mano = null;
	private int numeroJugadores = 1;
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	public Blackjack() {	
	}

	public ArrayList<Naipe> getCrupier() {
		return crupier;
	}

	public void setCrupier(ArrayList<Naipe> crupier) {
		this.crupier = crupier;
	}

	public ArrayList<ArrayList<Naipe>> getJugadores() {
		return jugadores;
	}

	public void setJugadores(ArrayList<ArrayList<Naipe>> jugadores) {
		this.jugadores = jugadores;
	}
	
	private void repartirCrupier() {
		mano = new Mano("Blackjack");	
		mano.setComenzadaMano(true);
		setCrupier(new ArrayList<Naipe>());
		getCrupier().add(mano.remove());
		getCrupier().add(mano.remove());
		getCrupier().get(0).setCartaVista(true);
	}
	
	private void repartirJugadores() {
		setJugadores(new ArrayList<ArrayList<Naipe>>());
		for (int i = 0; i < getNumeroJugadores(); i++) {
			getJugadores().add(new ArrayList<Naipe>());
		}
		
		for (ArrayList<Naipe> jugador : getJugadores()) {
			jugador.add(mano.remove());
			jugador.add(mano.remove());
			jugador.get(0).setCartaVista(true);
			jugador.get(1).setCartaVista(true);
		}
	}

	private void repartir() throws Exception {
		if (mano != null && mano.isComenzadaMano()) {
			throw new Exception("Partida ya comenzada");
		}
		repartirCrupier();
		repartirJugadores();
		
		mostrarCartarCroupier();
		mostrarCartasJugador(0);
	}
	
	private void pedir(int numeroJugador) throws Exception {
		if (mano == null || mano.isComenzadaMano() == false) {
			throw new Exception("Mano finalizada o no se ha repartido");
		}
		getJugadores().get(numeroJugador).add(mano.remove());
		getJugadores().get(numeroJugador).get(getJugadores().get(0).size()-1).setCartaVista(true);
		
		mostrarCartasJugador(0);
	}
	
	private void plantarse(int numeroJugador) throws Exception {
		if (mano == null || mano.isComenzadaMano() == false) {
			throw new Exception("Mano finalizada o no se ha repartido");
		}
		mostrarCartarCroupier();
		mostrarCartasJugador(0);
		mano.setComenzadaMano(false);
	}
	
	private void mostrarCartarCroupier() {
		System.out.println("Cartas croupier");
		int valorTotal = 0;
		for (Naipe naipe : getCrupier()) {
			System.out.println(naipe.toString());
			valorTotal+=naipe.getValor();
		}
		System.out.println("Valor total cartas: " + valorTotal);
	}
	
	private void mostrarCartasJugador(int numeroJugador) {
		System.out.println("Cartas jugador numero " + numeroJugador);
		int valorTotal = 0;
		for (Naipe naipe : getJugadores().get(numeroJugador)) {
			System.out.println(naipe.toString());
			valorTotal+=naipe.getValor();
		}
		System.out.println("Valor total cartas: " + valorTotal);
	}
	
	private void fin() throws Exception {
		if (mano != null && mano.isComenzadaMano()) {
			boolean respuesta = true;
			String cadenaFin;
			while (respuesta) {
				System.out.println("Desea acabar la partida [S/N]? Hay una mano comenzada");
				cadenaFin = br.readLine();
				
				if (cadenaFin.toLowerCase().equals("s")) {
					System.out.println("Finalizamos con la mano comenzada");
					this.seguir = false;
					respuesta = false;
				} else if (cadenaFin.toLowerCase().equals("n")) {
					System.out.println("Continuar mano comenzada");
					respuesta = false;
				} 
			}
		} else {
			System.out.println("Finalizar aplicacion");
			this.seguir = false;
		}
	}
	
	public boolean isSeguir() {
		return seguir;
	}

	public void setSeguir(boolean seguir) {
		this.seguir = seguir;
	}

	public int getNumeroJugadores() {
		return numeroJugadores;
	}

	public void setNumeroJugadores(int numeroJugadores) {
		this.numeroJugadores = numeroJugadores;
	}

	private void leerCadena(String cadena) throws Exception {
		String[] cadenaSplit = cadena.split(" ");
		
		if (cadenaSplit.length > 1) {
			throw new Exception("Error: Demasiados parametros");
		}
		
		String accion = cadenaSplit[0].toLowerCase();
		
		switch (accion) {
		case "repartir":
			repartir();
			break;
		case "pedir":
			pedir(0);
			break;
		case "plantarse":
			plantarse(0);
			break;
		case "fin":
			fin();
			break;
		default:
			throw new Exception("Accion no valida");
		}
	}

	public static void main(String[] args) {
		
		Blackjack b = new Blackjack();
			while (b.seguir) {
				System.out.print("Blackjack> ");
				try {
					b.leerCadena(b.br.readLine());
				} catch (Exception e) {
					 System.out.println(e);
				}
			}
	}
}
