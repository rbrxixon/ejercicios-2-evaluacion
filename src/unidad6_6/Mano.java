package unidad6_6;

import unidad5.Mazo;

public class Mano extends Mazo {

	private boolean comenzadaMano;
	
	public Mano(String juego) {
		super(juego);
	}

	public boolean isComenzadaMano() {
		return comenzadaMano;
	}

	public void setComenzadaMano(boolean comenzadaMano) {
		this.comenzadaMano = comenzadaMano;
	}
	
}
