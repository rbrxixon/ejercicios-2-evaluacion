package unidad6;

import java.util.Random;

public class Mascota {
	enum EnumEstado {
		Alegria,
		Apatia,
		Malestar
	}
	
	String nombre;
	int energia;
	EnumEstado estado;
	boolean enferma;

	public Mascota(String nombre) {
		super();
		this.nombre = nombre;
		this.energia = 20;
		this.enferma = false;
		this.estado = EnumEstado.Alegria;
	}

	private EnumEstado getEstado() {
		return estado;
	}

	private void setEstado(EnumEstado estado) {
		this.estado = estado;
	}

	private String getNombre() {
		return nombre;
	}

	private void setNombre(String nombre) {
		this.nombre = nombre;
	}

	private int getEnergia() {
		return energia;
	}

	private void setEnergia(int energia) {
		this.energia = energia;
	}
	
	private void cambiarEnergia(int energia) {
		setEnergia(energia);
		comprobarSiEnferma();
		actividadYaEnferma();
		comprobarEstado();
		//comprobarSiMuere();
	}

	private boolean isEnferma() {
		return enferma;
	}

	private void setEnferma(boolean enferma) {
		this.enferma = enferma;
	}

	@Override
	public String toString() {
		return "Mascota [nombre=" + nombre + ", energia=" + energia + ", estado=" + estado + ", enferma=" + enferma
				+ "]";
	}

	public boolean comer() {
		System.out.println("Comer, +5 de salud");
		cambiarEnergia(getEnergia() + 5);
		// Random para ponerse malo, probabilidad de 3 entre 10, si ocurre baja 10 unidadades
		// Calculo del 1 al 100, si sale un numero menor de 33, ocurre, sino no ocurre
		float numeroGenerado = 1 + new Random().nextFloat() * (1 - 100);
		if (numeroGenerado > 33.3) {
			System.out.println("Le ha sentado mal la comida, -10 de salud");
			cambiarEnergia(getEnergia() - 10 );
		}
		return comprobarSiMuere();
	}
	
	public boolean ejercicio() {
		System.out.println("Ejercicio, -3 de salud");
		cambiarEnergia(getEnergia() - 3);		
		return comprobarSiMuere();
	}
	
	public boolean dormir() {
		System.out.println("Dormir, +2 de salud");
		cambiarEnergia(getEnergia() + 2);
		return comprobarSiMuere();
	}
	
	public void curar() {
		if (isEnferma()) {
			System.out.println("Curar, 20 de salud");
			setEnferma(false);
			cambiarEnergia(20);
		} else {
			System.out.println("No es posible curar ya que no estaba enferma");
		}
	}
	
	private void actividadYaEnferma() {
		if (isEnferma()) {
			System.out.println("Las mascotas enfermas no deben realizar ninguna actividad hasta que la medicacion recetada las cure, -1 de salud");
			setEnergia(getEnergia() - 1);
		}
	}
	
	private void comprobarSiEnferma() {
		if (getEnergia() > 50) {
			System.out.println("Nivel de energia ha superado las 50 unidades por exceso de comida o descanso");
			setEnferma(true);
		} else if (getEnergia() < 5) {
			System.out.println("Su nivel de energia baja de las 5 unidades por exceso de actividad fisica");
			setEnferma(true);
		}
	}
	
	private boolean comprobarSiMuere() {
		if (getEnergia() < 0 || getEnergia() > 55) {
			System.out.println("La mascota ha muerto");
			return true;
		}
		return false;
	}
	
	private void comprobarEstado() {
		if (isEnferma() == false && (getEnergia() > 3) && (getEnergia() < 46)) {
			System.out.println("Alegria");
			setEstado(EnumEstado.Alegria);
		} else if (isEnferma() == false && (getEnergia() <= 3) && (getEnergia() >= 47) ) {
			System.out.println("Apatia");
			setEstado(EnumEstado.Apatia);
		} else if (isEnferma()) {
			System.out.println("Malestar");
			setEstado(EnumEstado.Malestar);
		}
	}
}
