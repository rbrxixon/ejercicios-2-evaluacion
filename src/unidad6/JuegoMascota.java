package unidad6;

import java.util.ArrayList;
import java.util.Scanner;

public class JuegoMascota {
	ArrayList<Mascota> mascotas;
	boolean jugar;
	
	public JuegoMascota() {
		super();
		this.mascotas = new ArrayList<Mascota>();
		jugar = true;
	}
	
	private ArrayList<Mascota> getMascotas() {
		return mascotas;
	}

	private void setMascotas(ArrayList<Mascota> mascotas) {
		this.mascotas = mascotas;
	}

	private boolean isJugar() {
		return jugar;
	}

	private void setJugar(boolean jugar) {
		this.jugar = jugar;
	}

	private void comprobarCadenaConsola(String cadena) throws MascotaException {
		
		String[] cadenaArray = cadena.split(" ");
		
		String accion = cadenaArray[0];
		String nombre = null;
		if (cadenaArray.length > 1)
			nombre = cadenaArray[1];
		
			Mascota m = null;
			
			switch (accion.toLowerCase()) {
			case "crear":
				System.out.println("Accion Crear");
				if (comprobarExistenciaNombre(nombre)) {
					m = new Mascota(nombre);
					getMascotas().add(m);
					System.out.println("Mascota creada " + m.toString());
				}
				break;
			case "comer":
				System.out.println("Accion Comer");
				m = comprobarNombre(nombre);
				if (m != null) {
					if (m.comer())
						getMascotas().remove(m);
				}
				break;
			case "ejercicio":
				System.out.println("Accion Ejercicio");
				m = comprobarNombre(nombre);
				if (m != null) {
					if (m.ejercicio())
						getMascotas().remove(m);
				}
				break;
			case "dormir":
				System.out.println("Accion Dormir");
				m = comprobarNombre(nombre);
				if (m != null) {
					if (m.dormir())
						getMascotas().remove(m);
				}
				break;
			case "curar":
				System.out.println("Accion Curar");
				m = comprobarNombre(nombre);
				if (m != null) {
					m.curar();
				}
				break;
			case "salir":
				System.out.println("Accion Salir");
				setJugar(false);
				break;
			default:
				throw new MascotaException("Accion desconocida");
				//break;
			}
	}
	
	private boolean comprobarExistenciaNombre(String nombre) throws MascotaException {
		if (nombre == null) {
			throw new MascotaException("Nombre no indicado");
		}
		for (Mascota mascota : getMascotas()) {
			if (mascota.nombre.toLowerCase().equals(nombre.toLowerCase())) {
				//System.out.println("Nombre ya escogido");
				throw new MascotaException("Nombre ya escogido");
				//return false;
			}
		}
		return true;
	}
	
	private Mascota comprobarNombre(String nombre) throws MascotaException {
		if (nombre == null) {
			//System.out.println("Nombre no indicado");
			throw new MascotaException("Nombre no indicado");
		}
		Mascota m = null;
		for (Mascota mascota : getMascotas()) {
			if (mascota.nombre.toLowerCase().equals(nombre.toLowerCase())) {
				m = mascota;
			}
		}
		
		if (m == null) {
			//System.out.println("Nombre de mascota no existe");			
			throw new MascotaException("Nombre de mascota no existe");
		} 
		return m;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		JuegoMascota juegoMascota = new JuegoMascota();
		
		System.out.println("Escribe comando");
		while (juegoMascota.isJugar()) {

			try {
				juegoMascota.comprobarCadenaConsola(sc.nextLine());
			} catch (MascotaException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		sc.close();
	}

}
