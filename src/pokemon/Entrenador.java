package pokemon;

import java.util.ArrayList;

public class Entrenador {
	String nombre;
	int insignias;
	ArrayList<Pokemon> coleccion;
	
	public Entrenador(String nombre, ArrayList<Pokemon> coleccion) {
		super();
		this.nombre = nombre;
		this.insignias = 0;
		this.coleccion = coleccion;
		System.out.println("Creado entrenador");
		System.out.println(toString());
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getInsignias() {
		return insignias;
	}

	public void setInsignias(int insignias) {
		this.insignias = insignias;
	}

	public ArrayList<Pokemon> getColeccion() {
		return coleccion;
	}

	public void setColeccion(ArrayList<Pokemon> coleccion) {
		this.coleccion = coleccion;
	}

	@Override
	public String toString() {
		return "Entrenador [nombre=" + nombre + ", insignias=" + insignias + ", coleccion=" + coleccion + "]";
	}

}
