package pokemon;

public class Pokemon {
	String nombre;
	ElementosEnum elemento;
	int salud;

	enum ElementosEnum {
		Agua,
		Fuego,
		Tierra,
		Electricidad,
		Veneno,
		Viento;
	}

	public Pokemon(String nombre, ElementosEnum elemento, int salud) {
		super();
		this.nombre = nombre;
		this.elemento = elemento;
		this.salud = salud;
		System.out.println("Creado pokemon");
		System.out.println(toString());
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ElementosEnum getElemento() {
		return elemento;
	}

	public void setElemento(ElementosEnum elemento) {
		this.elemento = elemento;
	}

	public int getSalud() {
		return salud;
	}

	public void setSalud(int salud) {
		this.salud = salud;
	}

	@Override
	public String toString() {
		return "Pokemon [nombre=" + nombre + ", elemento=" + elemento + ", salud=" + salud + "]";
	}
	
}
