package pokemon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

import pokemon.Pokemon.ElementosEnum;

public class Principal {
	ArrayList<Entrenador> entrenadores = null;
	
	private void crearEntrenador(String nombreEntrenador, String nombrePokemon, ElementosEnum elementoPokemon, int saludPokemon) {
		ArrayList<Pokemon> coleccion = null;
		coleccion = new ArrayList<Pokemon>();
		coleccion.add(new Pokemon(nombrePokemon, elementoPokemon, saludPokemon));
		entrenadores.add(new Entrenador(nombreEntrenador, coleccion));
	}
	
	private void torneo(ElementosEnum elemento) {
		Iterator<Pokemon> it = null;
		
		for (Entrenador entrenador : entrenadores) {
			it = entrenador.getColeccion().iterator();
			
			while (it.hasNext()) {
				Pokemon pokemon = it.next();
				if (elemento.equals(pokemon.elemento)) {
					entrenador.setInsignias(entrenador.getInsignias() + 1);
				} else {
					if (pokemon.getSalud() == 10) {
						System.out.println("Pokemon muerto");
						System.out.println(pokemon.toString());
						it.remove();
					} else {
						System.out.println("- 10 de salud");
						pokemon.setSalud(pokemon.getSalud() - 10);
						System.out.println(pokemon.toString());
					}
				}
			}
		}
	}
	
	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		Principal principal = new Principal();
		principal.entrenadores = new ArrayList<Entrenador>();
		boolean encontrado = false;
		boolean torneo = false;
		String console = null;
		System.out.println("Escribe en el input el comando");

		do {
			try {
				console = reader.readLine();
				
				String[] palabras = console.split("\\s");
				
				if (torneo) {
					System.out.println("Torneo = TRUE");
					if (console.toLowerCase().equals("agua")) {
						System.out.println("Agua!");
						principal.torneo(ElementosEnum.Agua);
					}
					
					if (console.toLowerCase().equals("fuego")) {
						System.out.println("Fuego!");
						principal.torneo(ElementosEnum.Fuego);
					}
					
					if (console.toLowerCase().equals("tierra")) {
						System.out.println("Tierra!");
						principal.torneo(ElementosEnum.Tierra);
					}
					
					if (console.toLowerCase().equals("electricidad")) {
						System.out.println("Electricidad!");
						principal.torneo(ElementosEnum.Electricidad);
					}
					
					if (console.toLowerCase().equals("veneno")) {
						System.out.println("Veneno!");
						principal.torneo(ElementosEnum.Veneno);
					}
					
					if (console.toLowerCase().equals("viento")) {
						System.out.println("Viento!");
						principal.torneo(ElementosEnum.Viento);
					}
				} else if (console.toLowerCase().equals("torneo")) {
					System.out.println("Torneo!");
					torneo = true;
				} else if (palabras.length == 4) {
					String nombreEntrenador = palabras[0];
					ElementosEnum elementoPokemon = null;
					String elementoPokemonString =  palabras[2];
					int saludPokemon = Integer.parseInt(palabras[3]);		
					
					switch (elementoPokemonString) {
						case "agua":
							elementoPokemon = ElementosEnum.Agua;
							break;
						case "fuego":
							elementoPokemon = ElementosEnum.Fuego;
							break;
						case "tierra":
							elementoPokemon = ElementosEnum.Tierra;
							break;
						case "electricidad":
							elementoPokemon = ElementosEnum.Electricidad;
							break;
						case "veneno":
							elementoPokemon = ElementosEnum.Veneno;
							break;
						case "viento":
							elementoPokemon = ElementosEnum.Viento;
							break;
						default:
							System.out.println("Error en el elemento");
							break;
						}
					
					if (principal.entrenadores.size() == 0) {
						System.out.println("Aun no existen entrenadores");
						principal.crearEntrenador(nombreEntrenador, palabras[1], elementoPokemon, saludPokemon); 
					} else {
						Entrenador entrenadorEncontrado = null;
						for (Entrenador entrenador : principal.entrenadores) {
							encontrado = false;
							System.out.println("FOR  " + entrenador.toString());
							if (entrenador.nombre.toLowerCase().equals(nombreEntrenador.toLowerCase())) {
								encontrado = true;
								entrenadorEncontrado = entrenador;
								break;
							} 
						}
						if (encontrado) {
							System.out.println("Entrenador existe");
							System.out.println(entrenadorEncontrado.toString());
							entrenadorEncontrado.getColeccion().add(new Pokemon(palabras[1], elementoPokemon, saludPokemon));
							System.out.println(entrenadorEncontrado.toString());
						} else {
							System.out.println("Entrenador no existe");
							principal.crearEntrenador(nombreEntrenador, palabras[1], elementoPokemon, saludPokemon);
						}
					}	
				} 		
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} while ( ! console.equals("fin"));
		

		for (Entrenador entrenador : principal.entrenadores) {
			System.out.println(entrenador.toString());
		}
		
	}

}
