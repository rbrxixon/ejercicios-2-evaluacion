package unidad_6_2;

import java.util.Scanner;

public class Ejercicio3 {
	
	boolean seguir = true;
	double a = 0;
	double b = 0;
	
	private void leerComando(String s) throws Exception {
		String[] cadena = s.split(" ");
		
		switch (cadena[0].toLowerCase()) {
		case "a":
			if (cadena.length != 2) {
				throw new Exception("Falta longitud");
			}
			try {
				a = Double.parseDouble(cadena[1]);
			} catch (NumberFormatException e) {
				System.out.println("Indica un double");
			} finally {
				System.out.println("A = " + a + " Establecida la longitud de uno de los catetos");
			}
			
			break;
		case "b":
			System.out.println("B: Establece la longitud del otro cateto");
			if (cadena.length != 2) {
				throw new Exception("Falta longitud");
			}
			
			try {
				b = Double.parseDouble(cadena[1]);
			} catch (NumberFormatException e) {
				System.out.println("Indica un double");
			} finally {
				System.out.println("B = " + b + " Establecida la longitud de uno de los catetos");
			}
			break;
		case "c":
			System.out.println(calcular(a, b));
			break;
		case "f":
			System.out.println("F: Finaliza el programa");
			this.seguir = false; 
			break;
		default:
			throw new Exception("No entiendo el comando");
		}
	}
	
	private String calcular(double a, double b) {
		double resultado = Math.sqrt(Math.pow(a, 2) * Math.pow(b, 2));
		this.a = 0;
		this.b = 0;
		return ("C: El valor de la hipotenusa es: " + resultado);
	}

	public static void main(String[] args) {
		Ejercicio3 e = new Ejercicio3();
		Scanner sc = new Scanner(System.in);
		
		while (e.seguir) {
			try {
			System.out.println();
			System.out.print("> ");
			e.leerComando(sc.nextLine());
			} catch (Exception ee) {
				ee.printStackTrace();
			}
		}
		sc.close();
	}
}
