package unidad_6_2; 

public class Ejercicio1 {
	
	public static boolean isInt(String n) {
		int numero;
		try {
			numero = Integer.parseInt(n);
			System.out.println("Numero " + numero);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return false;
		}
	}
	
	public static boolean isDouble(String n) {
		double numero;
		try {
			numero = Double.parseDouble(n);
			System.out.println("Numero " + numero);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return false;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		isInt("5");
		isInt("hola");
		
		isDouble("5.0");
		isInt("adios");
	}

}
