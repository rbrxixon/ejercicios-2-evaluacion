package unidad7_2;

public class Television extends Electrodomestico {
	private int tamanio;
	private EnumtipoSintonizador enumTipoSintonizador;
	public enum EnumtipoSintonizador {
		TIPO1("DVB-T"), 
		TIPO2("DVB-T2");
		
		private String tipo;

		public String getTipo() {
			return tipo;
		}

		private EnumtipoSintonizador(String tipo) {
			this.tipo = tipo;
		}
	}
	
	public Television(int precioBase, EnumColor enumColor, char consumoEnergetico, int peso) {
		super(precioBase, enumColor, consumoEnergetico, peso);
		this.tamanio = 20;
		this.enumTipoSintonizador = EnumtipoSintonizador.TIPO1;
		this.precioFinal = this.calcularPrecioFinal();
	}
	
	public Television(int precioBase, EnumColor enumColor, char consumoEnergetico, int peso, int tamanio, EnumtipoSintonizador enumTipoSintonizador) {
		super(precioBase, enumColor, consumoEnergetico, peso);
		this.tamanio = tamanio;
		this.enumTipoSintonizador = enumTipoSintonizador;
		this.precioFinal = this.calcularPrecioFinal();
	}
	
	public Television(int tamanio, EnumtipoSintonizador enumTipoSintonizador) {
		super();
		this.tamanio = tamanio;
		this.enumTipoSintonizador = enumTipoSintonizador;
		this.precioFinal = this.calcularPrecioFinal();
	}

	public int getTamanio() {
		return tamanio;
	}

	public void setTamanio(int tamanio) {
		this.tamanio = tamanio;
	}

	public EnumtipoSintonizador getEnumTipoSintonizador() {
		return enumTipoSintonizador;
	}

	public void setEnumTipoSintonizador(EnumtipoSintonizador enumTipoSintonizador) {
		this.enumTipoSintonizador = enumTipoSintonizador;
	}

	@Override
	public String toString() {
		return "Television [tamanio=" + tamanio + ", enumTipoSintonizador=" + enumTipoSintonizador.getTipo() + ", precioBase="
				+ precioBase + ", enumColor=" + enumColor + ", consumoEnergetico=" + consumoEnergetico + ", peso="
				+ peso + ", precioFinal=" + precioFinal + "]";
	}
}
