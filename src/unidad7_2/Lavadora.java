package unidad7_2;

public class Lavadora extends Electrodomestico {
	private int carga;
	
	public enum EnumCargas {
		CARGA_4(4),
		CARGA_5(5),
		CARGA_6(6),
		CARGA_7(7),
		CARGA_8(8),
		CARGA_10(10),
		CARGA_11(11),
		CARGA_13(13);
		
		private int valor;

		public int getValor() {
			return valor;
		}

		private EnumCargas(int valor) {
			this.valor = valor;
		}	
	}
	
	public Lavadora(int precioBase, EnumColor enumColor, char consumoEnergetico, int peso) {
		super(precioBase, enumColor, consumoEnergetico, peso);
		this.carga = EnumCargas.CARGA_5.getValor();
		this.precioFinal = this.calcularPrecioFinal();
	}
	
	public Lavadora(int precioBase, EnumColor enumColor, char consumoEnergetico, int peso, EnumCargas carga) {
		super(precioBase, enumColor, consumoEnergetico, peso);
		this.carga = carga.getValor();
		this.precioFinal = this.calcularPrecioFinal();
	}
	
	public Lavadora(EnumCargas carga) {
		super();
		this.carga = carga.getValor();
		this.precioFinal = this.calcularPrecioFinal();
	}

	public int getCarga() {
		return carga;
	}

	public void setCarga(int carga) {
		this.carga = carga;
	}
	

	@Override
	public String toString() {
		return "Lavadora [carga=" + carga + ", precioBase=" + precioBase + ", enumColor=" + enumColor
				+ ", consumoEnergetico=" + consumoEnergetico + ", peso=" + peso + ", precioFinal=" + precioFinal + "]";
	}

	@Override
	public double calcularPrecioFinal() {
		// TODO Auto-generated method stub
		if (this.carga > 8) {
			return super.calcularPrecioFinal(10);
		} else {
			return super.calcularPrecioFinal();
		}
	}

	
	
	
}
