package unidad7_2;

import java.util.ArrayList;

import unidad7_2.Lavadora.EnumCargas;
import unidad7_2.Television.EnumtipoSintonizador;

public class Tienda {
	private ArrayList<Electrodomestico> electrodomesticos = null;
	
	private Tienda() {
		this.electrodomesticos = new ArrayList<Electrodomestico>();
	}

	private ArrayList<Electrodomestico> getElectrodomesticos() {
		return electrodomesticos;
	}
	
	public double pruebaPorcentaje(int porcentaje) {
		int precioBase = 200;
		return precioBase*porcentaje/100 + precioBase;
	}

	public static void main(String[] args) {
		Tienda t = new Tienda();
			
		t.getElectrodomesticos().add(new Lavadora(EnumCargas.CARGA_13));
		t.getElectrodomesticos().add(new Lavadora(EnumCargas.CARGA_5));
		t.getElectrodomesticos().add(new Television(24, EnumtipoSintonizador.TIPO2));
		t.getElectrodomesticos().add(new Frigorifico(true));
		
		for (Electrodomestico electrodomestico : t.getElectrodomesticos()) {
			System.out.println(electrodomestico.toString());		
		}
	}

}
