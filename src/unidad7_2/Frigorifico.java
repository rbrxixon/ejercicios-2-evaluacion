package unidad7_2;

public class Frigorifico extends Electrodomestico {
	private boolean noFrost;
	
	public Frigorifico(int precioBase, EnumColor enumColor, char consumoEnergetico, int peso) {
		super(precioBase, enumColor, consumoEnergetico, peso);
		this.noFrost = false;
		this.precioFinal = this.calcularPrecioFinal();
	}
	
	public Frigorifico(int precioBase, EnumColor enumColor, char consumoEnergetico, int peso, boolean noFrost) {
		super(precioBase, enumColor, consumoEnergetico, peso);
		this.noFrost = noFrost;
		this.precioFinal = this.calcularPrecioFinal();
	}
	
	public Frigorifico(boolean noFrost) {
		super();
		this.noFrost = noFrost;
		this.precioFinal = this.calcularPrecioFinal();
	}

	public boolean isNoFrost() {
		return noFrost;
	}

	public void setNoFrost(boolean noFrost) {
		this.noFrost = noFrost;
	}
	
	@Override
	public String toString() {
		return "Frigorifico [noFrost=" + noFrost + ", precioBase=" + precioBase + ", enumColor=" + enumColor
				+ ", consumoEnergetico=" + consumoEnergetico + ", peso=" + peso + ", precioFinal=" + precioFinal + "]";
	}	
}
