package unidad7_2;

public abstract class Electrodomestico {
	protected int precioBase;
	protected EnumColor enumColor;
	protected char consumoEnergetico;
	protected int peso;
	protected double precioFinal;
	protected enum EnumColor {
		BLANCO,
		NEGRO,
		ROJO,
		AZUL,
		GRIS
	}
	
	public Electrodomestico() {
		super();
		this.precioBase = 100;
		this.enumColor = EnumColor.BLANCO;
		this.consumoEnergetico = 'F';
		this.peso = 5;
	}
	
	public Electrodomestico(int precioBase, EnumColor enumColor, char consumoEnergetico, int peso) {
		super();
		this.precioBase = precioBase;
		this.enumColor = enumColor;
		this.consumoEnergetico = consumoEnergetico;
		this.peso = peso;
	}

	public int getPrecioBase() {
		return precioBase;
	}
	
	public void setPrecioBase(int precioBase) {
		this.precioBase = precioBase;
	}
	
	public EnumColor getEnumColor() {
		return enumColor;
	}
	
	public void setEnumColor(EnumColor enumColor) {
		this.enumColor = enumColor;
	}
	
	public char getConsumoEnergetico() {
		return consumoEnergetico;
	}
	
	public void setConsumoEnergetico(char consumoEnergetico) {
		this.consumoEnergetico = consumoEnergetico;
	}
	
	public int getPeso() {
		return peso;
	}
	
	public void setPeso(int peso) {
		this.peso = peso;
	}
	
	public double getPrecioFinal() {
		return precioFinal;
	}
	
	public void setPrecioFinal(double precioFinal) {
		this.precioFinal = precioFinal;
	}
	
	public double calcularPrecioFinal() {
		int porcentaje = 0;
		switch (this.consumoEnergetico) {
			case 'A':
				porcentaje+=30;
				break;
			case 'B':
				porcentaje+=25;
				break;
			case 'C':
				porcentaje+=20;
				break;
			case 'D':
				porcentaje+=15;
				break;
			case 'E':
				porcentaje+=10;
				break;
			case 'F':
				porcentaje+=5;
				break;
			default:
				System.out.println("Error");
				break;
		}
		
		if (this.peso < 19) {
			porcentaje+=5;
		} else if ((this.peso >= 20) && (this.peso <= 49)) {
			porcentaje+=10;
		} else if ((this.peso >= 50) && (this.peso <= 79)) {
			porcentaje+=15;
		} else {
			porcentaje+=20;
		}
		return this.precioBase*porcentaje/100 + this.precioBase;
	}
	
	public double calcularPrecioFinal(int porcentajeAdicional) {
		int porcentaje = 0;
		switch (this.consumoEnergetico) {
			case 'A':
				porcentaje+=30;
				break;
			case 'B':
				porcentaje+=25;
				break;
			case 'C':
				porcentaje+=20;
				break;
			case 'D':
				porcentaje+=15;
				break;
			case 'E':
				porcentaje+=10;
				break;
			case 'F':
				porcentaje+=5;
				break;
			default:
				System.out.println("Error");
				break;
		}
		
		if (this.peso < 19) {
			porcentaje+=5;
		} else if ((this.peso >= 20) && (this.peso <= 49)) {
			porcentaje+=10;
		} else if ((this.peso >= 50) && (this.peso <= 79)) {
			porcentaje+=15;
		} else {
			porcentaje+=20;
		}
		
		porcentaje+=porcentajeAdicional;
		return this.precioBase*porcentaje/100 + this.precioBase;
	}

	@Override
	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", enumColor=" + enumColor + ", consumoEnergetico="
				+ consumoEnergetico + ", peso=" + peso + ", precioFinal=" + precioFinal + "]";
	}
	
	
}
